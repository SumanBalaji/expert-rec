import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { AuthGuardService } from './socialauth.service';

const routes: Routes = [{
  path:'',pathMatch: 'full', component:LoginComponent,
},
{path: 'mainpage', component: MainpageComponent, canActivate:[AuthGuardService]},
{
  path: '**', redirectTo: '', component:LoginComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
