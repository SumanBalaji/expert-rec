import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService, SocialUser } from 'angularx-social-login';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.scss']
})
export class MainpageComponent implements OnInit {
  user: SocialUser;
  loggedIn: boolean;


  constructor(private router:Router,public socialAuthServive:SocialAuthService) { }

  ngOnInit(): void {
    this.socialAuthServive.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
    });
  }
 
  logout(){
    this.socialAuthServive.signOut();
    this.router.navigate(['login'])
  }
}
